package cl.fiscalia.demoSpring.api;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ContactoFindRequest {

	@NotNull(message="El RUT es requerido")
	@Pattern(regexp="(^0?[1-9]{1,2})(?>((\\.\\d{3}){2}\\-)|((\\d{3}){2}\\-)|((\\d{3}){2}))([\\dkK])$", message="El RUT no está correctamente formado")
	private String rut;
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	
}