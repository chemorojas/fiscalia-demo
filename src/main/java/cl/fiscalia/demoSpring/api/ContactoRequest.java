package cl.fiscalia.demoSpring.api;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ContactoRequest {

	private Long id; 
	
	@NotNull(message="El RUT es requerido")
	@Pattern(regexp="(^0?[1-9]{1,2})(?>((\\.\\d{3}){2}\\-)|((\\d{3}){2}\\-)|((\\d{3}){2}))([\\dkK])$", message="El RUT no está correctamente formado")
	private String rut;
	
	@NotNull(message="El nombre es requerido")
	@Size(min=2, max=30, message="El nombre debe tener entre {min} y {max} caracteres")
	private String nombre;
	private String apellido;
	
	@Pattern(regexp="^\\+[0-9]*$", message="El número de telefono sólo puede tener dígitos iniciando con el símbolo +")
	private String telefono;
	private String email;
	
	public Long getId() { 
		return id; 
	} 
	
	public String getRut() {
		return rut;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getEmail() {
		return email;
	}
	
	public void setId(Long id) { 
		this.id = id; 
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}