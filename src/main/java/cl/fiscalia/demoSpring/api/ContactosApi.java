package cl.fiscalia.demoSpring.api;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.fiscalia.demoSpring.dto.Contacto;
import cl.fiscalia.demoSpring.service.ContactoService;

@RestController
public class ContactosApi {

	@Autowired
	ContactoService contactoService;
	
	// Inyecta mapper de Dozer
	@Autowired
	Mapper mapper;
	
	@RequestMapping(value="/grabar", method=RequestMethod.POST)
	public ContactoResponse save(@RequestBody @Valid ContactoRequest req){
		// Mapeo request dto ==> entity
		Contacto contact = mapper.map(req, Contacto.class);
	    
		// Invoca lógica de negocio
		Contacto updatedContact = contactoService.grabar(contact);
	    
		// Mapeo entity ==> response dto
	    ContactoResponse resp = mapper.map(updatedContact, ContactoResponse.class); 
	    
	    return resp;
	}
	
	@RequestMapping(value="/buscar", method=RequestMethod.POST)
	public ContactoResponse find(@RequestBody @Valid ContactoFindRequest req){
		// Mapeo request dto ==> entity
		Contacto contact = mapper.map(req, Contacto.class);
	    
		// Invoca lógica de negocio
		Contacto updatedContact = contactoService.buscarRut(contact);
	    
		// Mapeo entity ==> response dto
	    ContactoResponse resp = mapper.map(updatedContact, ContactoResponse.class); 
	    
	    return resp;
	}

}
