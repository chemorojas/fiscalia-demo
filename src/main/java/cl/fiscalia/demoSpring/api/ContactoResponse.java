package cl.fiscalia.demoSpring.api;

public class ContactoResponse {

	Long id; 
	String rut;
	String nombre;
	String apellido;
	String telefono;
	String email;

	public Long getId() { 
		return id; 
	} 
	
	public String getRut() {
		return rut;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getEmail() {
		return email;
	}
	
	public void setId(Long id) { 
		this.id = id; 
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
