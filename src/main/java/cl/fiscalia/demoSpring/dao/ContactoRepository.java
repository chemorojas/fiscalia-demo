package cl.fiscalia.demoSpring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.fiscalia.demoSpring.dto.Contacto;

public interface ContactoRepository extends JpaRepository<Contacto, Long> {

	List<Contacto> findByRut(String rut);
}
