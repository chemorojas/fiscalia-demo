package cl.fiscalia.demoSpring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import cl.fiscalia.demoSpring.dao.ContactoRepository;
import cl.fiscalia.demoSpring.dto.Contacto;

@Service
public class ContactoService {
	@Autowired
    ContactoRepository dao;

	@HystrixCommand(fallbackMethod = "errorGrabar")
    public Contacto grabar(@RequestBody Contacto contacto){
        return dao.saveAndFlush(contacto);
    }
	
	public Contacto errorGrabar(@RequestBody Contacto contacto){
        return new Contacto(0L,"0-0","Error al grabar contacto","","","");
    }

	@HystrixCommand(fallbackMethod = "errorBuscar")
	public Contacto buscarRut(@RequestBody Contacto contacto) {
		return dao.findByRut(contacto.getRut()).get(0); //la lista devolverá siempre un valor, ya que no existen dos personas con el mismo RUT
	}
	
	public Contacto errorBuscar(@RequestBody Contacto contacto){
        return new Contacto(0L,"0-0","Error al buscar contacto","","","");
    }
}